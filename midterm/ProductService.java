/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pin.midterm;

import java.util.ArrayList;
import java.util.Collection;

/**
 *
 * @author USER
 */
public class ProductService {

    private static ArrayList<Product> ProductList = new ArrayList<>();

    public static boolean addProduct(Product product) {
        ProductList.add(product);
        return true;
    }

    public static boolean Product(Product product) {
        ProductList.remove(product);
        return true;
    }

    public static boolean delProduct(int index) {
        ProductList.remove(index);
        return true;
    }

    public static ArrayList<Product> getProduct() {
        return ProductList;
    }

    public static Product getProduct(int index) {
        return ProductList.get(index);
    }

    public static boolean updateProduct(int index, Product product) {
        ProductList.set(index, product);

        return true;
    }

    public static boolean clearProduct() {
        ProductList.remove(ProductList);
        return true;
    }

    public static int totalPrice() {
        int sum = 0;
        for (int i = 0; i < ProductList.size(); i++) {
            sum += (ProductList.get(i).getPrice() * ProductList.get(i).getAmount());
        }
        return sum;
    }
    public static int totalAmount(){
        int sum =0;
        for(int i=0;i<ProductList.size();i++){
            sum+=ProductList.get(i).getAmount();
        }return sum;
    }
}

