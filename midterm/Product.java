/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pin.midterm;

/**
 *
 * @author USER
 */
public class Product {

    private int id;
    private String name;
    private String brand;
    private int price;
    private int amount;

    Product(int id,String name, String brand, int price, int amount) {
          this.id = id;
        this.name = name;
        this.brand = brand;
        this.price = price;
        this.amount = amount;
    }
   public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return  "id = " + id + ",name =" + name + ", brand =" + brand + ", price =" + price
                + ", amount =" + amount + '}';
    }

}
